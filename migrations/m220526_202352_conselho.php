<?php


use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_202352_conselho
 */
class m220526_202352_conselho extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_conselho',[
            'id'=>Schema::TYPE_PK,
            'condominio'=>Schema::TYPE_INTEGER,
            'nome'=>Schema::TYPE_STRING,
            'funcao'=>"ENUM('sindico','subSindico','conselheiro')"
        ]);
        $this->addForeignKey('from_condCon','jz_conselho','condominio','jz_condominio','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('from_condCon','jz_conselho');
        $this->dropTable('jz_conselho');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_202352_conselho cannot be reverted.\n";

        return false;
    }
    */
}
