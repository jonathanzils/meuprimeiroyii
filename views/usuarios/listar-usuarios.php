
<?
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$url_site = url::base($schema = true);
?>

<div class="col-12 text-center">
    <h1>Lista de Usuarios</h1>
</div>
<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaUsuario">
            <th>Nome do usuario</th>
            <th>usuario</th>
          
            <?
           
            foreach($usuarios as $ch=>$value){
                ?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nome']?></td>
                <td><?=$value['usuario']?></td>
                
               
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="">&nbsp;</td>
               
                <td colspan="2" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$paginacao->totalCount?></small></td>

            </tr>
        </table>
        <div class="row">
            <?=LinkPager::widget(
                [
                'pagination'=>$paginacao,
                'linkContainerOptions'=>[
                    'class'=>'page-item'
                ],
                'linkOptions'=>[
                    'class'=>'page-link'
                ],
                'disabledListItemSubTagOptions'=>[
                    'class'=>'page-link'                
                ]
                ]
            )
                ?>
    </div>
    </div>
</div>
