<?

use app\controllers\CondominiosController;
use app\controllers\MoradoresController;
use yii\helpers\Url;

?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro de Pet</h1>
    </div>
</div>

<form class="form-row formPets" action="<?echo Url::to(['pets/realiza-cadastro-pets'])?>" method="POST">
   
    <div class="col-12 col-md-12 from-group mt-2">
        <input class="col-12  text-center mb-2 bg-custom form-control" type="text" name="nomeDoPet" placeholder="Nome do Pet" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
    <select class="col-12 text-center bg-custom custom-select" name="tipo">
        <option>tipo</option>
        <option value="cachorro">Cachorro</option>
        <option value="gato">Gato</option>
        <option value="passaro">Passaro</option>
    </select>
    </div>
    <div class="col-12 col-md-12 text-center from-group mt-2 mb-2">
        <select name="condominio"class="fromMorCond col-12 bg-custom text-center custom-select">
            <option>Condominio</option>
            <?foreach(CondominiosController::listaCondominiosSelect() as $ch=>$val){?>
            <option value="<?=$val['id']?>"><?=$val['nomeDoCondominio']?></option>
            <?}?>
        </select>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
    <select class="col-12 text-center bg-custom custom-select fromMor" name="morador">
      
    </select>
    </div>

    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    
    <div class="col-12 col-md-12 text-center">
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
