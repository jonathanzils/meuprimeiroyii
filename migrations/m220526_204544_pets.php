<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_204544_pets
 */
class m220526_204544_pets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_pets',[
            'id'=>Schema::TYPE_PK,
            'nomeDoPet'=>Schema::TYPE_STRING,
            'morador'=>Schema::TYPE_INTEGER,
            'tipo'=>"ENUM('cachorro','gato','passaro')",
            'condominio'=>Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey('from_morPet','jz_pets','morador','jz_morador','id');
        $this->addForeignKey('from_condPets','jz_pets','condominio','jz_condominio','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('from_condPets','jz_pets');
        $this->dropForeignKey('from_morPet','jz_pets');
        $this->dropTable('jz_pets');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_204544_pets cannot be reverted.\n";

        return false;
    }
    */
}
