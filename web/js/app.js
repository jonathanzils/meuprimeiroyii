$(function(){
   //getMorador from condomonio
   $(document).on('change', '.fromMorCond',function(){
    selecionado = $(this).val();

    $.ajax({
        url: '?r=moradores/lista-moradores-api',
        dataType: 'json',
        type: 'POST',
        data: { id:selecionado },
        success : function(data){
            selectPopulation('.fromMor',data,'nome');
        }    
    })
})


    //getCond
    $(document).on('change', '.fromCondominio',function(){
            selecionado = $(this).val();
    
            $.ajax({
                url: '?r=blocos/lista-bloco-api',
                dataType: 'json',
                type: 'POST',
                data: { id:selecionado },
                success : function(data){
                    selectPopulation('.fromBloco',data,'nomeDoBloco');
                }    
            })
        })
        //getMoradorCond
        $(document).on('change', '.fromMor',function(){
            selecionado = $(this).val();
    
            $.ajax({
                url: '?r=blocos/lista-bloco-api',
                dataType: 'json',
                type: 'POST',
                data: { id:selecionado },
                success : function(data){
                    selectPopulation('.fromBloco',data,'nomeDoBloco');
                }    
            })
        })
        //getBloc
        $(document).on('change','.fromBloco',function(){
            selecionado = $(this).val();
            
            $.ajax({
                url: '?r=unidades/lista-unidade-api',
                dataType: 'json',
                type: 'POST',
                data: { id:selecionado },
                success : function(data){
                    selectPopulation('.fromUnidade',data,'nomeDaUnidade');
                }    
            })
        })
    
        function selectPopulation(seletor, dados, field){
            
            estrutura = '<option value="">selecione...</option>';
            for (let i = 0; i < dados.length; i++) {
        
                estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>'
        
            }
        
            $(seletor).html(estrutura);        
        }
    
         //modal
        $('.openModal').click(function(){
            caminho = $(this).attr('href');
            $(".modal-body").load(caminho, function(reponse,status){
                if(status ==='success'){
                    $('#modalComponent').modal({show:true});
                }
            }); 
            return false;         
        })
        //mascaras -cpf
        maskCpf = function(){$('input[name=cpf]').mask('999.999.999-99', {reverse:true})}
        $(document).on('click, focus','input[name=cpf]', function(){
            maskCpf()
        })
        //mascaras -telefone
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
          spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
          };
        $(document).on('click, focus','input[name=telefone]',function(){telefoneMask()})
        telefoneMask = function(){$('input[name=telefone]').mask(SPMaskBehavior, spOptions)};
        //mascaras -cnpj
        $(document).on('click, focus','input[name=cnpj]',function(){cnpjMask()})
        cnpjMask = function(){$('input[name=cnpj]').mask('99.999.999/999-99')}
        //mascara -cep
        $(document).on('click, focus','input[name=cep]', function(){cepMask()})
        cepMask = function(){$('input[name=cep]').mask('99999-999')}



      
        //autoComplete
        autocomplete = function(cep){

            $.ajax({
                url: `https://viacep.com.br/ws/${cep}/json/`,
                method: 'GET',
                dataType: 'json',
                success: function(data){
                    $('.logradouro').val(data.logradouro)
                    $('.bairro').val(data.bairro)
                    $('.cidades').val(data.localidade)
                    $('.fromEstados').val(data.uf)               
                }
            })
        }
        //autocomplemento pelo cep
        $(document).on('keyup','.cep',function(){
            let cep = $(this).val();
            console.log(cep);
            if(cep.length == 8 || cep.length == 9){
                autocomplete(cep)
            }
            
        })
    });
    
    
    function myAlert(tipo, mensagem, pai, url) {
        url = (url == undefined) ? url == '' : url = url;
        componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>';
    
        $(pai).prepend(componente);
    
        setTimeout(function () {
            $(pai).find('div.alert').remove()
            //redirecionar?
            if (tipo == 'success' && url) {
                setTimeout(function () {
                    window.location.href = url;
                }, 500);
            }
        }, 2000)
    }