<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formUsuario" action="<?= Url::to(['usuarios/realiza-cadastro-usuario'])?>" method="POST">
   

    <div class="col-12 col-md-8 text-right mt-2">
        <input class="col-6 text-center mb-2" type="text" name="g[nome]" placeholder="Nome do Usuario" value=""required>
    </div>

    <div class="col-12 col-md-8 text-right mt-2">
        <input class="col-6 text-center" type="text" name="g[usuario]" placeholder="Usuario"value=""required>
    </div>
    <div class="col-12 col-md-8 text-right mt-2">
        <?=$inputSenha?>
    </div>
    <div class="col-12 col-md-8 text-right mt-2">
       <?=$inputConfSenha?>
    </div>
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
        
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>