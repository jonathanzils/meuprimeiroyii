<?
namespace app\controllers;

use app\components\MascaraComponent;
use app\models\AdministradorasModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominiosModel;
use yii;
use app\controllers\UserLogado;

class CondominiosController extends Controller{
    

    public function actionListarCondominios(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        
      
       
        $query = (new \yii\db\Query())
        ->select(['cond.id', 'cond.nomeDoCondominio', 'cond.bloco', 'cond.logradouro', 'cond.numero','cond.bairro', 'cond.cidade', 'cond.estado', 'cond.cep', 'adm.nomeDaAdm AS adm'])
        ->from(CondominiosModel::tableName().' cond')
        ->innerJoin(AdministradorasModel::tableName().' adm','adm.id = cond.adm');
    
        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);



        $Condominios = $query->orderBy('nomeDoCondominio')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-condominios', [
            'condominios'=>$Condominios,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroCondominios(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        
        return $this->render('cadastro-condominios');
    }
    public function actionRealizaCadastroCondominios(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new CondominiosModel();
            foreach($request->post() as $field=>$value){
                if($field == 'cep'){
                    $getMany[$field] = mascaraComponent::desMask($value);
                }else{
                    $getMany[$field] = $value;
                }
            }
            $model->attributes = $getMany();
            $model->save();
            return $this->redirect(['condominios/listar-condominios']);
        }
        return $this->render(['cadastro-condominios']);
    }
    public static function listaCondominiosSelect(){
        
        $query = CondominiosModel::find();
        return $query->orderBy("nomeDoCondominio")->all();
    }
    public function actionEditaCondominios(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = CondominiosModel::find();
            $condominios = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-condominios',[
            'edit'=>$condominios
        ]);
    }
    public function actionRealizaEdicaoCondominios(){
        $request = \yii::$app->request; 
        if($request->isPost){
            
            $model = CondominiosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
         
            if($model->update()){
               return $this->redirect(['condominios/listar-condominios']);
           }else{
               return $this->redirect(['condominios/listar-condominios','msg'=>'erro']);
           }
            

        }

    }
    public function actionDeletaCondominios(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = CondominiosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['condominios/listar-condominios',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['condominios/listar-condominios',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    public static function numeroDeMoradores(){
        $query = yii::$app->db->createCommand('SELECT
        cond.nomeDoCondominio AS conds,
        COUNT(mor.id) AS mors
        FROM
        jz_morador mor
        INNER JOIN jz_condominio cond ON cond.id = mor.condominio
        GROUP BY conds')->queryAll();
        return $query;
    }
    //retorna o total de condominios cadastrados
    public static function totalConds(){
        $query = yii::$app->db->createCommand('SELECT
        COUNT(id)   
        FROM jz_condominio')->queryScalar();
        return $query;
    }
    
    //pesquisa por nome do cond?
    public function actionSearchCond(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\Query())
            ->select(['cond.id', 'cond.nomeDoCondominio', 'cond.bloco', 'cond.logradouro', 'cond.numero','cond.bairro', 'cond.cidade', 'cond.estado', 'cond.cep', 'adm.nomeDaAdm AS adm'])
            ->from(CondominiosModel::tableName().' cond')
            ->innerJoin(AdministradorasModel::tableName().' adm','adm.id = cond.adm')
            ->where(['like','nomeDoCondominio', $request->get('termo')]);
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $Condominios = $query->orderBy('nomeDoCondominio')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();


        }
        return $this->render('listar-condominios',[
            'condominios'=>$Condominios,
            'paginacao'=>$paginacao
        ]);
    }
    
    
}
?>