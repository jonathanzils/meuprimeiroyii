<?

use app\components\selectedComponent;
use yii\helpers\url;
use app\controllers\CondominiosController;
use app\controllers\MoradoresController;

?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Edição</h1>
    </div>
</div>

<form class="form-row formPets" action="<?=Url::to(['pets/realiza-edicao-pets'])?>" method="POST">
    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="condominio" class="col-6 text-center custom-select bg-custom fromMorCond">
            <option>Condominio</option>
            <? foreach (CondominiosController::listaCondominiosSelect() as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>"<?= selectedComponent::isSelected($val['id'],$edit['condominio']) ?>><?=$val['nomeDoCondominio']?></option>
            <? } ?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="morador" class="fromMor col-6 text-center custom-select bg-custom">
        <? foreach (MoradoresController::moradoresSelect($edit['condominio']) as $mor){?>
              <option value="<?= $mor['id'] ?>"<?= selectedComponent::isSelected($mor['id'],$edit['morador']) ?>><?= $mor['nome'] ?></option>
        <?}?>

        </select>
    </div>

    <div class="col-12 text-center">
        <input class="col-6 bg-custom form-control text-center" type="text" name="nomeDoPet" placeholder="Nome do pet" value="<?=$edit['nomeDoPet']?>" required>
    </div>
    <div class="col-12 text-center">
        <input class="col-6  bg-custom form-control text-center" type="text" name="tipo" placeholder="Tipo do pet" value="<?=$edit['tipo']?>" required>
    </div>

    

    
    
    
    <div class="col-12 text-center">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    </div>

    <div class="col-12 col-md-12 text-center">
        
       
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>

    </div>
</form>