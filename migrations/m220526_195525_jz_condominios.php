<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_195525_jz_condominios
 */
class m220526_195525_jz_condominios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_condominio',[
            'id'=>Schema::TYPE_PK,
            'nomeDoCondominio'=>Schema::TYPE_STRING,
            'bloco'=>Schema::TYPE_INTEGER,
            'logradouro'=>Schema::TYPE_STRING,
            'numero'=>Schema::TYPE_INTEGER,
            'bairro'=>Schema::TYPE_STRING,
            'cidade'=>Schema::TYPE_STRING,
            'estado'=>Schema::TYPE_STRING,
            'cep'=>Schema::TYPE_STRING,
            'adm'=>Schema::TYPE_INTEGER,
        ]);
        $this->addForeignKey('from_adm','jz_condominio','adm','jz_adm','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('from_adm','jz_condominio');
       $this->dropTable('jz_condominio');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_195525_jz_condominios cannot be reverted.\n";

        return false;
    }
    */
}
