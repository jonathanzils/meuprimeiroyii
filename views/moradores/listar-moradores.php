<?

use app\components\alertComponent;
use app\components\mascaraComponent;
use app\components\ModalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

// $leg = yii::$app->legivelComponent;
// $leg->legivel($moradores);


$url_site = url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}
?>


<div class="col-12 text-center">
    <h1>Lista de pessoas</h1>
</div>
<form id="filtro" method="GET" class="form-row mt-5 justify-content-center">
    <div class="col-12 col-sm-10">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text text-white bg-dark">Nome Do Morador:</div>
        </div>
        <input type="hidden" name="r" value="moradores/search-moradores">
        <input type="search"aria-label="Search" class="form-control" name="termo">
      </div>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Pesquisar</button>
        <a href="<?=$url_site?>?r=moradores/listar-moradores" class="btn btn-danger">Cancelar</a>
    </div>
    </form>
<div class="row">
  
    <div class="col-12 col-md-12">
        <table class="table table-responsive-md table-responsive-sm table-dark table-striped"id="listaClientes">
            <th>Condominio</th>
            <th>Bloco</th>
            <th>Unidade</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>Nascimento</th>
            <th>E-mail</th>
            <th style="width:150px;">Telefone</th>
            <th>DT. Cadastro/Ultima atualização</th>
           
            <th><a href="<?=$url_site?>?r=moradores/cadastro-moradores" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
            foreach($moradores as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['bloco']?></td>
                <td><?=$value['unidade']?></td>
                <td><?=$value['nome']?></td>
                <td style="white-space: nowrap;"><?=mascaraComponent::mascara($value['cpf'], 'cpf')?></td>
                <td><?=Yii::$app->formatter->format($value['nascimento'],'date')?></td>
                <td><?=$value['email']?></td>
                <td style="white-space: nowrap;"><?=mascaraComponent::mascara($value['telefone'], 'telefone')?></td>
                <td><?=Yii::$app->formatter->format($value['dataCadastro'],'date')?></td>
                <td>
                    <a href="<?=$url_site?>/index.php?r=moradores/edita-morador&id=<?=$value['id']?>"name="id" class="text-white openModal"><i class="bi bi-pen-fill"></i></a> 
                    <a href="<?=$url_site?>?r=moradores/deleta-moradores&id=<?=$value['id']?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerCliente"><i class="bi bi-trash3-fill"></i></a>
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="5">&nbsp;</td>
               
                <td colspan="6" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$paginacao->totalCount?></small></td>

            </tr>
        </table>
        <div class="row">
            <?=LinkPager::widget(
                [
                'pagination'=>$paginacao,
                'linkContainerOptions'=>[
                    'class'=>'page-item'
                ],
                'linkOptions'=>[
                    'class'=>'page-link'
                ],
                'disabledListItemSubTagOptions'=>[
                    'class'=>'page-link'                
                ]
                ]
            )
                ?>
    </div>
    </div>
</div>
<?=ModalComponent::modal()?>

