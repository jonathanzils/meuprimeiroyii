<?


use yii\helpers\Url;
use app\controllers\CondominiosController;


?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Cadastro</h1>
    </div>
</div>

<form class="form-row formCliente" action="<?=Url::to(['moradores/realiza-cadastro-moradores'])?>" method="POST">
    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="condominio" class="col-6 text-center custom-select bg-custom fromCondominio">
            <option>Condominio</option>
            <? foreach (CondominiosController::listaCondominiosSelect() as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>"><?= $val['nomeDoCondominio'] ?></option>
            <? } ?>
        </select>
    </div>

    <div class="col-12 col-md-6 text-center from-group mb-2">
        <select name="bloco" class="fromBloco col-12 text-center custom-select bg-custom">

        </select>
    </div>

    <div class="col-12 col-md-6 text-center from-group mb-2">
        <select name="unidade" class="fromUnidade col-12 text-center custom-select bg-custom">
      

        </select>
    </div>

    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom mb-2" type="text" name="nome" placeholder="Nome" value="" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="cpf" placeholder="cpf" value="" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="email" placeholder="E-mail" value="" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="telefone" placeholder="Telefone" value="">
    </div>
    
    <div class="col-12 col-md-12 text-center">
      <input type="date" name="nascimento" class="col-12 text-center mt-2 form-control bg-custom">
    </div>
  
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
        
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">Enviar</button>
       

    </div>
</form>