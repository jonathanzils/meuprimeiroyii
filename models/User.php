<?
namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{
    public static function tableName()
    {
        return 'jz_usuarios';
    }
    /**
     * este metodo procura por uma instancia da classe de identidade usando o ID do usuario especificado
     * este metodo e usado quando voce precisa manter o status de login via sessao 
     * @param[type]$id
     * @return void
     */

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    /**
     * ele procura por uma instancia da classe de identidade usando o otoken de acesso informado
     * este metodo e usado quando voce precisa autendicar um usario por um unico token secreto (exemplo:em uma aplicacao estaless restfull)
     */

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;//statis::findOne(['token'=>$token]);
    }
    /**
     * retorna o ID do usuario representado por essa instancia da classe de identidade
     * 
     * @return void
     */
    public function getId(){
        return $this->id;
    }
    /**
     * retorna uma chave para verificar login via cookie. A chave é mantida no cookie de login e sera comparada com a informação do lado do servidor
     * para testar a validade do cookie
     * @return void
     */
    public function getAuthKey(){
        return null;//$this->auth_key;
    }
    /**
     * implementa a logica de verificação da chave de login viad cookie;
     * @param[type]$authKey
     * @return void
     */
    public function validateAuthKey($authKey){
        return null;//$this->auth_key === $authKey;
    }
}
?>