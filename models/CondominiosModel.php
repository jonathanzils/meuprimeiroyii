<?
namespace app\models;

use yii\db\ActiveRecord;

class CondominiosModel extends ActiveRecord{
    public static function tablename(){
        return 'jz_condominio';
    }

    public function rules(){
        return [
            [['nomeDoCondominio','bloco','logradouro','numero','bairro','cidade','cep','adm'], 'required'],
        
        ];
    }
}

?>