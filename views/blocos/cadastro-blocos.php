<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

?>

<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formBloco" action="<?= Url::to(['blocos/realiza-cadastro-blocos'])?>" method="POST">
    <div class="col-12 col-md-12 text-center">
    <select class="col-12 text-center custom-select bg-custom" name="condominio">
           <option>condominio</option>
        <?foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
        <option value="<?=$cond['id']?>"><?=$cond['nomeDoCondominio'];?></option>
        <?}?>
       </select>
    </div>
    <div class="col-12 col-md-12 text-center mt-2">
        <input class="col-12 text-center mb-2 form-control bg-custom" type="text" name="nomeDoBloco" placeholder="Nome do bloco" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center">
        <input class="col-12 text-center form-control bg-custom" type="text" name="andar" placeholder="Quantos andares?" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center mt-2">
        <input class="col-12 text-center form-control bg-custom" type="text" name="unidades"placeholder="Quantidade de unidades por andar" value=""required>
    </div>
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
