<?
use app\components\alertComponent;
use yii\helpers\Url;

?>
<!DOCTYPE html>
<html lang="pt-BR" class="bg-dark">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" href="css/site.css">
    
    <title>projeto</title>

<body class="bg-dark">
    <main class="container">
        <div class="row mt-5">
            <div class="col-12 col-sm-12 text-center text-white justify-content-center login">
            <img src="images\Logo-site.png" class="mb-5" style="width: 250px">
            <h1>Efetue o login</h1>
        <form action="<?=URl::to(['site/login']);?>" method="POST">
            <div class="mb-3 d-flex justify-content-center text-center mt-5"> 
                <input type="text" class="form-control col-5"name="usuario" placeholder="Login">
            </div>
            <div class="mb-3 d-flex justify-content-center text-center">
                <input type="password" name="senha" class="form-control col-5"placeholder="senha">
            </div>
            <button type="submit" class="btn btn-warning col-4">Logar</button>

            <input type="hidden" name="<?=\yii::$app->request->csrfParam?>"value="<?=\yii::$app->request->csrfToken?>">
        </form>
        </div>

        </div>
            <?if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'], $_GET['myAlert']['url']);
    }?>
        
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
</main>
</body>

</html>