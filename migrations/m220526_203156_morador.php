<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_203156_morador
 */
class m220526_203156_morador extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('jz_unidade',[
            'id'=>Schema::TYPE_PK,
            'condominio'=>Schema::TYPE_INTEGER,
            'bloco'=>Schema::TYPE_INTEGER,
            'nomeDaUnidade'=>Schema::TYPE_STRING,
            'metragem'=>Schema::TYPE_INTEGER,
            'vagas'=>Schema::TYPE_INTEGER,
            'dataCadastro'=>Schema::TYPE_TIMESTAMP
        ]);
        $this->addForeignKey('from_condUnid','jz_unidade','condominio','jz_condominio','id');
        $this->addForeignKey('from_blocoUnid','jz_unidade','bloco','jz_bloco','id');



        $this->createTable('jz_morador',[
            'id'=>Schema::TYPE_PK,
            'condominio'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'bloco'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'unidade'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'nome'=>Schema::TYPE_STRING . ' NOT NULL',
            'cpf'=>Schema::TYPE_STRING . ' NOT NULL',
            'nascimento'=>Schema::TYPE_DATETIME,
            'email'=>Schema::TYPE_STRING . ' NOT NULL',
            'email'=>$this->string(11),
            'dataCadastro'=>Schema::TYPE_TIMESTAMP

        ]);
        $this->addForeignKey('from_condmor','jz_morador','condominio','jz_condominio','id');
        $this->addForeignKey('from_blocomor','jz_morador','bloco','jz_bloco','id');
        $this->addForeignKey('from_unidmor','jz_morador','unidade','jz_unidade','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('from_unidmor','jz_morador');
        $this->dropForeignKey('from_blocomor','jz_morador');
        $this->dropForeignKey('from_condmor','jz_morador');

        $this->dropForeignKey('from_blocoUnid','jz_unidade');
        $this->dropForeignKey('from_condUnid','jz_unidade');
        


        $this->dropTable('jz_unidade');
        $this->dropTable('jz_morador');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_203156_morador cannot be reverted.\n";

        return false;
    }
    */
}
