<?
namespace app\controllers;

use PHPUnit\Framework\Constraint\Count;
use yii\web\Controller;

class maskController extends controller{
    
    public static function mask($val, $format){
        $maskared = '';
        $k = 0;
        switch($format){
            case 'cpf':
                $mask = '###.###.###-##';
                break;
            case 'cnpj':
                $mask = '##.###.###/###-##';
                break;
            case 'cep':
                $mask = '####-###';
                break;
            case 'telefone':
                if(strlen($val) == 11){
                    $mask = '(##) #####-####';
                }else{$mask = '(##) ####-####';}
            default:
            break;
        }
        if($val == null){
            return "--";
        }

        for ($i=0; $i <=strlen($mask)-1 ; $i++) { 
            if($mask[$i] == '#'){
                if(isset($val[$k])){

                    $maskared .= $val[$k++];
                }
            }else{
                if(isset($mask[$i])){
                    $maskared .= $mask[$i];
                }
            }
        }
        return $maskared;
    }
    
    
    
    // public static function cpfMask($d){
    //     $str1 = substr($d,0,3).'.';
    //     $str2 = substr($d,3,3).'.';
    //     $str3 = substr($d,6,3).'-';
    //     $str4 = substr($d,9,3);

    //     return $str1.$str2.$str3.$str4;
        
    // }

    // public static function cnpjMask($d){
    //     $str1 = substr($d,0,2).'.';
    //     $str2 = substr($d,2,3).'.';
    //     $str3 = substr($d,5,3).'/';
    //     $str4 = substr($d,8,4).'-';
    //     $str5 = substr($d,12,2);

    //     return $str1.$str2.$str3.$str4.$str5;      

    // }
    // public static function cepMask($d){
    //     $str1 = substr($d,0,5).'-';
    //     $str2 = substr($d,5,3);

    //     return $str1.$str2;
    // }
    // public static function telefoneMask($d){
        
    //     $cont = strlen($d);
    //     if($cont == 11){
    //         $str1 = '(';
    //         $str2 = substr($d,0,2).')';
    //         $str3 = substr($d,2,5).'-';
    //         $str4 = substr($d,7,4);
    //     }else{
    //         $str1 = '(';
    //         $str2 = substr($d,0,2).')';
    //         $str3 = substr($d,2,4).'-';
    //         $str4 = substr($d,6,4);
    //     }


    //     return $str1.$str2.$str3.$str4;
    // }
}



?>