<?
namespace app\controllers;

use app\components\alertComponent;
use app\components\mascaraComponent;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use app\models\CondominiosModel;
use app\models\BlocosModel;
use yii;


class MoradoresController extends Controller{
    public function actionListarMoradores(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select(' mor.id,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco AS bloco,
        unid.nomeDaUnidade AS unidade,
        mor.nome,
        mor.cpf,
        mor.nascimento,
        mor.email,
        mor.telefone,
        mor.dataCadastro')
        ->from(MoradoresModel::tableName().' mor')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = mor.condominio')
        ->innerjoin(unidadesModel::tableName().' unid','unid.id = mor.unidade')
        ->innerjoin(blocosModel::tableName().' bloco','bloco.id = mor.bloco');



        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $moradores = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-moradores', [
            'moradores'=>$moradores,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroMoradores(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-moradores');
    }
    public function actionRealizaCadastroMoradores(){

        $request = \yii::$app->request;
    
        if($request->isPost){
            $model = new MoradoresModel();
            foreach($request->post() as $field=>$value){
                if($field == 'cpf' || $field == 'telefone'){
                    $getMany[$field] = mascaraComponent::desMask($value);
                }else{
                    $getMany[$field] = $value;
                }
            }
            $model->attributes = $getMany;
            $model->save();
            return $this->redirect(['moradores/listar-moradores']);
        }
        return $this->render(['cadastro-moradores']);
    }
    public function actionEditaMorador(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = MoradoresModel::find();

            $moradores = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-morador',[
            'edit'=>$moradores
        ]);
    }
    public function actionRealizaEdicaoMorador(){
        $request = \yii::$app->request; 
        if($request->isPost){ 
            $model = MoradoresModel::findOne($request->post('id'));
            foreach($request->post() as $field=>$value){
                if($field == 'cpf' || $field == 'telefone'){
                    $getMany[$field] = mascaraComponent::desMask($value);
                }else{
                    $getMany[$field] = $value;
                }
            }
            $model->attributes = $getMany;
            
         
            if($model->update()){
               return $this->redirect(['moradores/listar-moradores']);
           }else{
               return $this->redirect(['moradores/listar-moradores','msg'=>'erro']);
           }
            

        }

    }
    public function actionDeletaMoradores(){
        $request = \yii::$app->request;
        $model = MoradoresModel::findOne($request->get('id'));
        if($request->isGet){
            $model->delete();
        }
        try {
                    return $this->redirect(['moradores/listar-moradores',
                    'myAlert'=>[
                        'type'=>'success',
                        'msg'=>'Registro deletado com sucesso'
                    ]]);
                
               
        } catch (\Throwable $th) {
            return $this->redirect(['moradores/listar-moradores',
            'myAlert'=>[
                'type'=>'danger',
                'msg'=>'Não foi possivel deletar o registro'
            ]]);
        }
            
        
    }
    public static function listaMoradoresSelect(){
        $query = MoradoresModel::find();
        return $query->orderBy("nome")->all();
    }
    
    public static function moradoresSelect($from){
        $query = MoradoresModel::find();
        $data = $query->where(['condominio'=>$from])->orderBy('nome')->all();
        return $data;

    }
    public function actionListaMoradoresApi(){
        $request = \yii::$app->request;
        $query = MoradoresModel::find();
        $data = $query->where(['condominio'=>$request->post()])->orderBy('nome')->all();
        $dados = array();
        $i = 0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['nome'] = $d['nome'];
        $i++;
        }
        return json_encode($dados);
    }

  //retorna o total de moradores cadastrados
  public static function totalMors(){
      $query = yii::$app->db->createCommand('SELECT COUNT(id) FROM  jz_morador')->queryScalar();
      return $query;
  }

  public function actionSearchMoradores(){
    $request = \yii::$app->request;
    if($request->isGet){
       
        $query = (new \yii\db\Query())
        ->select(' mor.id,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco AS bloco,
        unid.nomeDaUnidade AS unidade,
        mor.nome,
        mor.cpf,
        mor.nascimento,
        mor.email,
        mor.telefone,
        mor.dataCadastro')
        ->from(MoradoresModel::tableName().' mor')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = mor.condominio')
        ->innerjoin(unidadesModel::tableName().' unid','unid.id = mor.unidade')
        ->innerjoin(blocosModel::tableName().' bloco','bloco.id = mor.bloco')
        ->where(['like','nome', $request->get('termo')]);

        
        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);



        $moradores = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->All();


    }
    return $this->render('listar-moradores',[
        'moradores'=>$moradores,
        'paginacao'=>$paginacao
    ]);
}
}
?>