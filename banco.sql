-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para jz_sistema
DROP DATABASE IF EXISTS `jz_sistema`;
CREATE DATABASE IF NOT EXISTS `jz_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `jz_sistema`;

-- Copiando estrutura para tabela jz_sistema.jz_adm
DROP TABLE IF EXISTS `jz_adm`;
CREATE TABLE IF NOT EXISTS `jz_adm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDaAdm` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_adm: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_adm` DISABLE KEYS */;
INSERT INTO `jz_adm` (`id`, `nomeDaAdm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'ADMINISTRA', '12457898653212', '2022-03-30 12:52:26'),
	(2, 'Killowpar', '65984512326598', '2022-03-30 12:52:45'),
	(3, 'antiadm', '54895632214598', '2022-03-30 12:52:59');
/*!40000 ALTER TABLE `jz_adm` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_bloco
DROP TABLE IF EXISTS `jz_bloco`;
CREATE TABLE IF NOT EXISTS `jz_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `nomeDoBloco` varchar(255) NOT NULL DEFAULT '',
  `andar` varchar(255) NOT NULL DEFAULT '',
  `unidades` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `FKcondominio` (`condominio`),
  CONSTRAINT `FKcondominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_bloco: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_bloco` DISABLE KEYS */;
INSERT INTO `jz_bloco` (`id`, `condominio`, `nomeDoBloco`, `andar`, `unidades`) VALUES
	(22, 57, 'Asd', '2', '2'),
	(23, 57, 'B', '2', '2'),
	(24, 56, 'F', '2', '2'),
	(25, 56, 'g', '2', '2'),
	(26, 58, 'e', '2', '2'),
	(27, 58, 'r', '2', '2');
/*!40000 ALTER TABLE `jz_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_condominio
DROP TABLE IF EXISTS `jz_condominio`;
CREATE TABLE IF NOT EXISTS `jz_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDoCondominio` varchar(255) NOT NULL DEFAULT '',
  `bloco` int(11) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT '',
  `numero` int(11) DEFAULT 0,
  `bairro` varchar(255) DEFAULT '',
  `cidade` varchar(255) DEFAULT '',
  `estado` varchar(255) DEFAULT '',
  `cep` varchar(50) DEFAULT NULL,
  `adm` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_jz_condominio_jz_adm` (`adm`),
  CONSTRAINT `FK_jz_condominio_jz_adm` FOREIGN KEY (`adm`) REFERENCES `jz_adm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_condominio: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_condominio` DISABLE KEYS */;
INSERT INTO `jz_condominio` (`id`, `nomeDoCondominio`, `bloco`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `adm`) VALUES
	(56, 'teste', 1, 'teste', 1213, 'teste', 'indaial', 'SC', '13247', 2),
	(57, 'cond grinaldo', 1, 'rua do gridnildo', 145, 'grivaldo', 'gridlandia', 'SC', '1235879', 3),
	(58, 'willie wonka', 2, 'wiliii', 666, 'wlliaka', 'willilandia', 'SC', '5898563', 1),
	(61, 'teste bsc', 2, 'sad', 2313, 'dasd', 'asasd', 'sc', '123123', 1);
/*!40000 ALTER TABLE `jz_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_conselho
DROP TABLE IF EXISTS `jz_conselho`;
CREATE TABLE IF NOT EXISTS `jz_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('sindico','subSindico','conselheiro') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jz_conselho_jz_condominio` (`condominio`),
  CONSTRAINT `FK_jz_conselho_jz_condominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_conselho: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_conselho` DISABLE KEYS */;
/*!40000 ALTER TABLE `jz_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_morador
DROP TABLE IF EXISTS `jz_morador`;
CREATE TABLE IF NOT EXISTS `jz_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `bloco` int(11) DEFAULT 0,
  `unidade` int(11) DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(255) NOT NULL DEFAULT '',
  `nascimento` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `keyCondominio` (`condominio`),
  KEY `keyBloco` (`bloco`),
  KEY `keyUnidade` (`unidade`),
  CONSTRAINT `keyBloco` FOREIGN KEY (`bloco`) REFERENCES `jz_bloco` (`id`),
  CONSTRAINT `keyCondominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`),
  CONSTRAINT `keyUnidade` FOREIGN KEY (`unidade`) REFERENCES `jz_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_morador: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_morador` DISABLE KEYS */;
INSERT INTO `jz_morador` (`id`, `condominio`, `bloco`, `unidade`, `nome`, `cpf`, `nascimento`, `email`, `telefone`, `dataCadastro`) VALUES
	(41, 57, 22, 23, 'ze', '111.111.111-11', '0000-00-00 00:00:00', 'contato@gmail.com', '1231313132', '2022-04-13 11:44:44'),
	(43, 57, 22, 24, 'jao', '333.333.333-33', '0000-00-00 00:00:00', 'contato@hottmail.com', 'contato@hot', '2022-04-13 12:30:06'),
	(44, 57, 23, 25, 'zézin', '555.555.555-55', '0000-00-00 00:00:00', 'contato@hottmail.com', '4788889635', '2022-04-13 12:31:19'),
	(45, 57, 23, 26, 'pao invertido', '777.777.777-77', '0000-00-00 00:00:00', 'contato@hottmail.com', '4855669852', '2022-04-14 07:39:29'),
	(49, 56, 25, 29, 'lupus', '000.000.000-00', '0000-00-00 00:00:00', 'contato@hottmail.com', '47991338568', '2022-04-13 12:44:51'),
	(50, 58, 26, 31, 'wonka', '000.000.000-00', '0000-00-00 00:00:00', 'contato@hottmail.com', '4733332856', '2022-04-13 12:46:07'),
	(51, 58, 26, 31, 'willie', '000.000.000-00', '0000-00-00 00:00:00', 'contato@hottmail.com', '4788523698', '2022-04-13 12:47:25'),
	(52, 58, 27, 33, 'lalalu', '000.000.000-00', '0000-00-00 00:00:00', 'contato@hottmail.com', '4788885269', '2022-04-13 12:47:49'),
	(53, 58, 27, 34, 'lolzao', '000.000.000-00', '0000-00-00 00:00:00', 'contato@hottmail.com', '4788596352', '2022-04-13 12:48:15');
/*!40000 ALTER TABLE `jz_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_pets
DROP TABLE IF EXISTS `jz_pets`;
CREATE TABLE IF NOT EXISTS `jz_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDoPet` varchar(50) NOT NULL DEFAULT '0',
  `morador` int(11) NOT NULL DEFAULT 0,
  `tipo` enum('cachorro','gato','passaro') NOT NULL,
  `condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jz_pets_jz_morador` (`morador`),
  KEY `FK_jz_pets_jz_condominio` (`condominio`),
  CONSTRAINT `FK_jz_pets_jz_condominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_jz_pets_jz_morador` FOREIGN KEY (`morador`) REFERENCES `jz_morador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_pets: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_pets` DISABLE KEYS */;
INSERT INTO `jz_pets` (`id`, `nomeDoPet`, `morador`, `tipo`, `condominio`) VALUES
	(3, 'toto', 43, 'cachorro', 57),
	(4, 'locao', 53, 'gato', 58);
/*!40000 ALTER TABLE `jz_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_unidade
DROP TABLE IF EXISTS `jz_unidade`;
CREATE TABLE IF NOT EXISTS `jz_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `bloco` int(11) DEFAULT 0,
  `nomeDaUnidade` varchar(255) NOT NULL DEFAULT '',
  `metragem` int(11) NOT NULL DEFAULT 0,
  `vagas` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`condominio`),
  KEY `chBloco` (`bloco`),
  CONSTRAINT `chBloco` FOREIGN KEY (`bloco`) REFERENCES `jz_bloco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_unidade: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_unidade` DISABLE KEYS */;
INSERT INTO `jz_unidade` (`id`, `condominio`, `bloco`, `nomeDaUnidade`, `metragem`, `vagas`, `dataCadastro`) VALUES
	(23, 57, 22, '101', 12, 1, '2022-04-13 11:03:22'),
	(24, 57, 22, '102', 25, 1, '2022-04-13 11:03:42'),
	(25, 57, 23, '202', 23, 2, '2022-04-13 11:04:07'),
	(26, 57, 23, '203', 23, 2, '2022-04-13 11:04:22'),
	(27, 56, 24, '303', 223, 2, '2022-04-13 11:04:39'),
	(28, 56, 25, '305', 23, 23, '2022-04-13 11:04:53'),
	(29, 56, 25, '506', 23, 23, '2022-04-13 11:05:05'),
	(30, 56, 24, '304', 23, 23, '2022-04-13 11:05:19'),
	(31, 58, 26, '708', 75, 2, '2022-04-13 11:05:30'),
	(32, 58, 26, '707', 23, 3, '2022-04-13 12:46:32'),
	(33, 58, 27, '808', 808, 8, '2022-04-13 11:05:54'),
	(34, 58, 27, '909', 23, 3, '2022-04-13 11:06:05');
/*!40000 ALTER TABLE `jz_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_usuarios
DROP TABLE IF EXISTS `jz_usuarios`;
CREATE TABLE IF NOT EXISTS `jz_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_usuarios` DISABLE KEYS */;
INSERT INTO `jz_usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(1, 'Zils', 'jo', '123', '2022-04-21 13:17:30');
/*!40000 ALTER TABLE `jz_usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
