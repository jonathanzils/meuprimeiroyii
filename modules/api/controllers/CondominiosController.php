<?
namespace app\modules\api\controllers;

use app\models\AdministradorasModel;
use app\models\CondominiosModel;

use yii\web\Controller;

class CondominiosController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    public static function actionAdmSelect(){
       $request = \yii::$app->request;
       $qry = CondominiosModel::find();

       $data = $qry->where(['adm'=>$request->get('adm')])->orderBy('nomeDoCondominio')->all();
       $dados = [];

       if($qry->count() > 0){
           $dados['endPoint']['status'] = 'success';
           $dados['totalResults'] = $qry->count();
           $i = 0;
           foreach ($data as $d) {
               $dados['resultSet'][$i]['id'] = $d['id'];
               $dados['resultSet'][$i]['nomeDocondominio'] = $d['nomeDoCondominio'];
               $i++;
           }
       }else{
           $dados['endPoint']['status'] = 'noData';
           $dados['endPoint']['msg'] = 'Não existe dados para este consumo';
       }
       return json_encode($dados);
    }
    
    public function actionGetAll(){
        $qry = (new \yii\db\Query())
        ->select(['cond.id', 'cond.nomeDoCondominio', 'cond.bloco', 'cond.logradouro', 'cond.numero','cond.bairro', 'cond.cidade', 'cond.estado', 'cond.cep', 'adm.nomeDaAdm AS adm','adm.id AS idAdm'])
        ->from(CondominiosModel::tableName().' cond')
        ->innerJoin(AdministradorasModel::tableName().' adm','adm.id = cond.adm');
        $data = $qry->orderBy('nomeDoCondominio')->all();
        $dados = [];
        $i = 0;
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
               foreach($d as $ch=>$val){
                   $dados['resultSet'][$i][$ch] = [$val];
               }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';       
        }
        return json_encode($dados);
    }
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
        ->select(['cond.id', 'cond.nomeDoCondominio', 'cond.bloco', 'cond.logradouro', 'cond.numero','cond.bairro', 'cond.cidade', 'cond.estado', 'cond.cep', 'adm.nomeDaAdm AS nomeAdm','adm.id AS adm'])
        ->from(CondominiosModel::tableName().' cond')
        ->innerJoin(AdministradorasModel::tableName().' adm','adm.id = cond.adm');
        $d = $qry->where(['cond.id' => $request->get('id')])->one();
       
      
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$val){
                $dados['resultSet'][0][$ch] = [$val];
            }
            return json_encode($dados);
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';       
        }
        return json_encode($dados);
    }
   
    public function actionRegisterCond(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new CondominiosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso';
                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'danger';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo.';
            return json_encode($dados);
        }
    }
    public function actionEditCond(){
        $request = \yii::$app->request;
        try {
            if($request->isPost){
                $model = CondominiosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro alterado com sucesso';
                return json_encode($dados);
            }
        } catch (\Throwable $th) {
            $dados = [];
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo.';
            return json_encode($dados);
        }
    }
    public function actionDeleteCond(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = CondominiosModel::findOne($request->post('id'));
                $model->delete();
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados = [];
            $dados['endPoint']['status'] = 'danger';
            $dados['endPoint']['msg'] = 'Não foi possivel deletar este registro.';
            return json_encode($dados);
        }
        
        
    }


    
}
?>