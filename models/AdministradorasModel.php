<?
namespace app\models;

use yii\db\ActiveRecord;

class AdministradorasModel extends ActiveRecord{

    public static function tablename(){
        return 'jz_adm';
    }
 
    public function rules(){
        return [
            [['nomeDaAdm','nomeDaAdm'], 'required'],
            [['cnpj','cnpj'], 'required']
        ];
    }
}

?>