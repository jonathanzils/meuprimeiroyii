<?
namespace app\controllers;

use app\models\CondominiosModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhosModel;
use yii;

class ConselhosController extends Controller{
    public function actionListarConselhos(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select('con.id,
        cond.nomeDoCondominio AS condominio,
        con.nome AS nome,
        con.funcao AS funcao')
        ->from(ConselhosModel::tableName().' con')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = con.condominio');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $conselhos = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-conselhos', [
            'conselhos'=>$conselhos,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroConselhos(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-conselhos');
    }
    public function actionRealizaCadastroConselhos(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new ConselhosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['conselhos/listar-conselhos']);
        }
        return $this->render(['cadastro-conselhos']);
    }
    public function actionEditaConselhos(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = ConselhosModel::find();
            $conselhos = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-conselhos',[
            'edit'=>$conselhos
        ]);
    }
    public function actionRealizaEdicaoConselhos(){
        $request = \yii::$app->request; 
        if($request->isPost){
            
            $model = ConselhosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
         
            if($model->update()){
               return $this->redirect(['conselhos/listar-conselhos']);
           }else{
               return $this->redirect(['conselhos/listar-conselhos','msg'=>'erro']);
           }
            

        }

    }
    public function actionDeletaConselhos(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = ConselhosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['conselhos/listar-conselhos',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['conselhos/listar-conselhos',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    public function actionSearchConselhos(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\Query())
            ->select('con.id,
            cond.nomeDoCondominio AS condominio,
            con.nome AS nome,
            con.funcao AS funcao')
            ->from(ConselhosModel::tableName().' con')
            ->innerJoin(CondominiosModel::tableName().' cond','cond.id = con.condominio')
            ->where(['like','cond.nomeDoCondominio', $request->get('termo')]);
    
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $conselhos = $query->orderBy('nomeDoCondominio')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();


        }
        return $this->render('listar-conselhos',[
            'conselhos'=>$conselhos,
            'paginacao'=>$paginacao
        ]);
    }
}
?>