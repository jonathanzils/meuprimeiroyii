<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadesModel;
use app\models\CondominiosModel;
use app\models\BlocosModel;
use yii;

class UnidadesController extends Controller{
    public function actionListarUnidades(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select(' unid.id,
        cond.nomeDoCondominio AS condominio,
        cond.id as idcond,
        bloco.nomeDoBloco AS bloco,
        unid.nomeDaUnidade,
        unid.metragem,
        unid.vagas,
        unid.dataCadastro')
        ->from(UnidadesModel::tableName().' unid')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = unid.condominio')
        ->innerjoin(blocosModel::tableName().' bloco','bloco.id = unid.bloco');


        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $unidades = $query->orderBy('nomeDaUnidade')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-unidades', [
            'unidades'=>$unidades,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroUnidades(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-unidades');
    }
    public function actionRealizaCadastroUnidades(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/listar-unidades']);
        }
        return $this->render(['cadastro-unidades']);
    }
    public static function unidadesSelect($from){
        $query = UnidadesModel::find();
        $data = $query->where(['bloco'=>$from])->orderBy('nomeDaUnidade')->all();
        return $data;

    }
    public function actionListaUnidadeApi(){
        $request = \yii::$app->request;
        $query = UnidadesModel::find();
        $data = $query->where(['bloco'=>$request->post()])->orderBy('nomeDaUnidade')->all();
        $dados = array();
        $i = 0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['nomeDaUnidade'] = $d['nomeDaUnidade'];
        $i++;
        }
        return json_encode($dados);
    }
    public function actionEditaUnidades(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = UnidadesModel::find();
            $unidades = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-unidades',[
            'edit'=>$unidades
        ]);
    }
    public function actionDeletaUnidades(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = unidadesModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['unidades/listar-unidades',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['unidades/listar-unidades',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    public function actionSearchUnidades(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\Query())
            ->select(' unid.id,
            cond.nomeDoCondominio AS condominio,
            cond.id as idcond,
            bloco.nomeDoBloco AS bloco,
            unid.nomeDaUnidade,
            unid.metragem,
            unid.vagas,
            unid.dataCadastro')
            ->from(UnidadesModel::tableName().' unid')
            ->innerJoin(CondominiosModel::tableName().' cond','cond.id = unid.condominio')
            ->innerjoin(blocosModel::tableName().' bloco','bloco.id = unid.bloco')
            ->where(['like','cond.nomeDoCondominio', $request->get('termo')]);
    
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $unidades = $query->orderBy('nomeDoCondominio')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();


        }
        return $this->render('listar-unidades',[
            'unidades'=>$unidades,
            'paginacao'=>$paginacao
        ]);
    }
}
?>