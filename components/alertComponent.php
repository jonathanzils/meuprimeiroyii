<?
namespace app\components;

use yii\base\component;

class alertComponent extends component{

        public static function myAlert($tipo, $msg, $url=''){
        $component = "<div class=\"alert alert-{$tipo}\" role=\"alert\">'{$msg}'</div>";
        $componentParent = "
        <script type=\"text/javascript\">
            url = '{$url}';
            tipo = '{$tipo}';
            setTimeout(function () {
                $('main').find('div.alert').remove()
                //redirecionar?
                if (tipo == 'success' && url) {
                 setTimeout(function () {
                        window.location.href = url;
                    }, 500);
             }
         }, 2000)
            </script>
            ";

        return $component . $componentParent;
    }
}


?>