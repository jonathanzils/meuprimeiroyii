<?


use app\components\mascaraComponent;
use app\components\selectedComponent;
use app\controllers\BlocosController;
use yii\helpers\url;
use app\controllers\CondominiosController;
use app\controllers\UnidadesController;
use app\models\UnidadesModel;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;

?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Edição</h1>
    </div>
</div>

<form class="form-row formCliente" action="<?=Url::to(['moradores/realiza-edicao-morador'])?>" method="POST">
    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="condominio" class="col-6 text-center custom-select bg-custom fromCondominio">
            <option>Condominio</option>
            <? foreach (CondominiosController::listaCondominiosSelect() as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>"<?= selectedComponent::isSelected($val['id'],$edit['condominio']) ?>><?=$val['nomeDoCondominio']?></option>
            <? } ?>
        </select>
    </div>

    <div class="col-12 col-md-6 text-center from-group mb-2">
        <select name="bloco" class="fromBloco col-12 text-center bg-custom custom-select">
        <? foreach (BlocosController::blocosSelect($edit['condominio']) as $blocos){?>
              <option value="<?= $blocos['id'] ?>"<?= selectedComponent::isSelected($blocos['id'],$edit['bloco']) ?>><?= $blocos['nomeDoBloco'] ?></option>
        <?}?>
               
           
        </select>
    </div>

    <div class="col-12 col-md-6 text-center from-group mb-2">
        <select name="unidade" class="fromUnidade col-12 text-center custom-select bg-custom">
        <? foreach (UnidadesController::unidadesSelect($edit['bloco']) as $unidade){?>
              <option value="<?= $unidade['id'] ?>"<?= selectedComponent::isSelected($unidade['id'],$edit['unidade']) ?>><?= $unidade['nomeDaUnidade'] ?></option>
        <?}?>

        </select>
    </div>

    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom mb-2" type="text" name="nome" placeholder="Nome" value="<?=$edit['nome']?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="cpf" placeholder="cpf" value="<?=mascaraComponent::mascara($edit['cpf'], 'cpf')?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="email" placeholder="E-mail" value="<?=$edit['email']?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center form-control bg-custom" type="text" name="telefone" placeholder="Telefone" value="<?=mascaraComponent::mascara($edit['telefone'],'telefone')?>">
    </div>
    
    <div class="col-12 col-md-12 mt-2 d-flex justify-content-center">
       
       <input type="date" class="form-control bg-custom col-12 col-sm-6 text-center"name="nascimento" value="<?=$edit['nascimento']?>">
    </div>
    <div class="col-12 text-center">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    </div>

    <div class="col-12 col-md-12 text-center">
        
       
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>

    </div>
</form>