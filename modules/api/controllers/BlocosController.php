<?
namespace app\modules\api\controllers;


use app\models\CondominiosModel;
use app\models\BlocosModel;
use Exception;
use yii\web\Controller;

class BlocosController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    public static function actionCondSelect(){
        $request = \yii::$app->request;
        $qry = blocosModel::find();
 
        $data = $qry->where(['condominio'=>$request->get('condominio')])->orderBy('nomeDoBloco')->all();
        $dados = [];
 
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach ($data as $d) {
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeDoBloco'] = $d['nomeDoBloco'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo';
        }
        return json_encode($dados);
     }

    public function actionGetAll(){
       
        $qry = (new \yii\db\Query())
        ->select('bloco.id,
        cond.id as idcond,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco,
        bloco.andar,
        bloco.unidades')
        ->from(BlocosModel::tableName().' bloco')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = bloco.condominio');

        $data = $qry->orderBy('nomeDoBloco')->all();
        $dados = [];
        $i = 0;
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
               foreach($d as $ch=>$val){
                   $dados['resultSet'][$i][$ch] = [$val];
               }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';       
        }
        return json_encode($dados);
    }
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
        ->select('bloco.id,
        cond.id as idcond,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco,
        bloco.andar,
        bloco.unidades')
        ->from(BlocosModel::tableName().' bloco')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = bloco.condominio');

        $d = $qry->where(['bloco.id' => $request->get('id')])->one();
       
      
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$val){
                $dados['resultSet'][0][$ch] = [$val];
            }
            return json_encode($dados);
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';       
        }
        return json_encode($dados);
    }
    
    public function actionRegisterBlocos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new BlocosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso';
                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'danger';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo.';
            return json_encode($dados);
        }
    }
    public function actionEditBlocos(){
        $request = \yii::$app->request;
        try {
            if($request->isPost){
                $model = BlocosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro alterado com sucesso';
                return json_encode($dados);
            }
        } catch (Exception $th) {
            $dados = [];
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = $th;
            return json_encode($dados);
        }
    }
    public function actionDeleteBlocos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = BlocosModel::findOne($request->post('id'));
                $model->delete();
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                return json_encode($dados);
            }

        } catch (\Throwable $th) {
            $dados = [];
            $dados['endPoint']['status'] = 'danger';
            $dados['endPoint']['msg'] = 'Não foi possivel deletar este registro.';
            return json_encode($dados);
        }
        
        
    }


    
}
?>