<?

use app\components\selectedComponent;
use yii\helpers\url;
use app\controllers\CondominiosController;

?>
<form class="form-row formConselho" action="<?= Url::to(['conselhos/realiza-edicao-conselhos'])?>" method="POST">

<div class="col-12 col-md-12 text-center from-group">
    <select name="condominio" class="col-6 text-center mb-2 custom-select bg-custom">
    <option>condominio</option>
    <? foreach (CondominiosController::listaCondominiosSelect() as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>"<?= selectedComponent::isSelected($val['id'],$edit['condominio']) ?>><?=$val['nomeDoCondominio']?></option>
            <? } ?>
</div> 

<div class="col-12 col-md-12 mt-2">
    <input class="col-12 text-center form-control bg-custom mb-2" type="text" name="nome" placeholder="nome" value=""required>
</div>
<div class="col-12 col-md-12">
<select name="funcao" class="col-12 text-center form-control mb-2 custom-select bg-custom">
    <option>função</option>
        <option value="1">Sindico</option>
        <option value="2">Sub sindico</option>
        <option value="3">Conselheiro</option>
    </select>
</div>
<input type="hidden" name="id" value="<?=$edit['id']?>">
<input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
<div class="col-12 col-md-12 text-center">

    <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
    
</div>
</form>