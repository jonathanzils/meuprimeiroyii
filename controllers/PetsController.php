<?
namespace app\controllers;

use app\models\CondominiosModel;
use app\models\MoradoresModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\PetsModel;
use yii;

class PetsController extends Controller{
    public function actionListarPets(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\query())
        ->select('
        pet.id as id,
        pet.nomeDoPet AS nomeDoPet,
        pet.tipo AS tipo,
        mor.nome AS morador,
        cond.nomeDoCondominio as condominio')
        ->from(PetsModel::tablename().' pet')
        ->innerjoin(MoradoresModel::tablename().' mor','mor.id = pet.morador')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = mor.condominio');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $pets = $query->orderBy('nomeDoPet')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-pets', [
            'pets'=>$pets,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroPets(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-pets');
    }
    public function actionRealizaCadastroPets(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new petsModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['pets/listar-pets']);
        }
        return $this->render(['cadastro-pets']);
    }
    public static function listapetsSelect(){
        $query = petsModel::find();
        return $query->orderBy("nomeDoPet")->all();
    }
    public function actionEditaPets(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = petsModel::find();
            $pets = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-pets',[
            'edit'=>$pets
        ]);
    }
    public function actionRealizaEdicaoPets(){
        $request = \yii::$app->request; 
        if($request->isPost){
            
            $model = PetsModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
         
            if($model->update()){
               return $this->redirect(['pets/listar-pets']);
           }else{
               return $this->redirect(['pets/listar-pets','msg'=>'erro']);
           }

        }
    }
    public function actionDeletaPets(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = PetsModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['pets/listar-pets',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['pets/listar-pets',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    public function actionSearchPets(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\query())
            ->select('
            pet.id as id,
            pet.nomeDoPet AS nomeDoPet,
            pet.tipo AS tipo,
            mor.nome AS morador,
            cond.nomeDoCondominio as condominio')
            ->from(petsModel::tablename().' pet')
            ->innerjoin(MoradoresModel::tablename().' mor','mor.id = pet.morador')
            ->innerJoin(CondominiosModel::tableName().' cond','cond.id = mor.condominio') 
            ->where(['like','mor.nome', $request->get('termo')]);
    
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $pets = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();
    
    
        }
        return $this->render('listar-pets',[
            'pets'=>$pets,
            'paginacao'=>$paginacao
        ]);
    }
}
?>