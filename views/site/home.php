<?

use app\controllers\AdministradorasController;
use app\controllers\CondominiosController;
use app\controllers\MoradoresController;
use app\models\AdministradorasModel;
use yii\helpers\Html;

?>
<script
src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
</script>

<h1 class="text-center mb-5">BEM VINDO A AP.CONTROLE</h1>

<div class="row justify-content-center mt-4 mb-4">
    <canvas id="myChart" style="width:100%;max-width:1000px"></canvas>

</div>

<div class="row">
    <div class="col-sm-6">
        <h3 class="text-center mt-5 mb-3">Administradoras</h3>
        <table class="table table-responsive-md table-dark table-striped text-center    ">
            <tr>
                <td>Administradora</td>
                <td>qtd. de Condominios</td>
            </tr>
            <?
            foreach(AdministradorasController::numeroDeConds() as $value){?>
            <tr>
                
              <td><?=$value['adms']?></td>
              <td><?=$value['conds']?></td>
            </tr>
                <?}?>
        </table>
    </div>

    <div class="col-sm-6">
        <h3 class="text-center mt-5 mb-3">Condominios</h3>
        <table class="table table-responsive-md table-dark table-striped text-center">
            <tr>
                <td>Condoiminio</td>
                <td>qtd. de Moradores</td>
            </tr>
            <?foreach(CondominiosController::numeroDeMoradores() as $value){?>
            <tr>
               <td><?=$value['conds']?></td>
               <td><?=$value['mors']?></td>
            </tr>
                <?}?>
        </table>
    </div>
</div>

<script>
var xValues = ["Condominios", "Moradores", "Administradoras"];
var yValues = [<?=CondominiosController::totalConds()?>,<?=MoradoresController::totalMors()?>,<?=AdministradorasController::totalAdms()?>,0];
var barColors = ["brown", "green", "blue"];

new Chart("myChart", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: "Grafico de cadastros 2022"
    }
  }
});
</script>