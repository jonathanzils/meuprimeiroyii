<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdministradorasModel;
use app\models\CondominiosModel;

use yii;

class AdministradorasController extends Controller{
    
    
    public function actionListarAdministradoras(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = AdministradorasModel::find();

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $administradoras = $query->orderBy('nomeDaAdm')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-administradoras', [
            'administradoras'=>$administradoras,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroAdministradoras(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-administradoras');
    }
    public function actionRealizaCadastroAdministradoras(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AdministradorasModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['administradoras/listar-administradoras']);
        }
        return $this->render(['cadastro-administradoras']);
    }
    public static function listaAdministradorasSelect(){
        $query = AdministradorasModel::find();
        return $query->orderBy("nomeDaAdm")->all();
    }
    public function actionEditaAdministradoras(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-administradoras',[
            'edit'=>$administradoras
        ]);
    }
    public function actionRealizaEdicaoAdministradoras(){
        $request = \yii::$app->request; 
        if($request->isPost){
            
            $model = AdministradorasModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
         
            if($model->update()){
               return $this->redirect(['administradoras/listar-administradoras']);
           }else{
               return $this->redirect(['administradoras/listar-administradoras','msg'=>'erro']);
           }

        }
    }
    public function actionDeletaAdministradoras(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = AdministradorasModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['administradoras/listar-administradoras',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['administradoras/listar-administradoras',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    //retorna o numero de condominios por adm
    public static function numeroDeConds(){
        $query = Yii::$app->db->createCommand('SELECT 
        adm.nomeDaAdm AS adms,
        COUNT(cond.id) AS conds
        FROM
        jz_condominio cond
        INNER JOIN jz_adm adm ON adm.id = cond.adm
        GROUP BY adms
        ')
        ->queryAll();
        
        return $query;
    }
    public function actionSearchAdm(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\Query())
            ->select('id,
            nomeDaAdm,
            cnpj,
            dataCadastro')
            ->from(administradorasModel::tableName())
            ->where(['like','nomeDaAdm', $request->get('termo')]);
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $administradoras = $query->orderBy('nomeDaAdm')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();


        }
        return $this->render('listar-administradoras',[
            'administradoras'=>$administradoras,
            'paginacao'=>$paginacao
        ]);
    }
    public static function totalAdms(){
        $query = yii::$app->db->createCommand('SELECT
        COUNT(id)   
        FROM jz_adm')->queryScalar();
        return $query;
    }
   
}
?>