<?

use app\components\ModalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\controllers\MaskController;

$url_site = url::base($schema = true);
?>


<div class="col-12 text-center">
    <h1>Lista de Administradoras</h1>
</div>
<form id="filtro" method="GET" class="form-row justify-content-center mt-5">
    <div class="col-12 col-sm-10 mb-2 ">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text text-white bg-dark">Nome Da Administradora:</div>
        </div>
        <input type="hidden" name="r" value="administradoras/search-adm">
        <input type="search"aria-label="Search" class="form-control" name="termo">
      </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Pesquisar</button>
        <a href="<?=$url_site?>?r=administradoras/listar-administradoras" class="btn btn-danger">Cancelar</a>
    </div>
</form>
<div class="row">
    <div class="col-12 col-md-12">
        <table class="table table-responsive-md table-dark table-striped"id="listaAdm">
            <th>Administradora</th>
            <th>CNPJ</th>
            <th>Dt. cadastro/atualização</th>
            <th><a href="<?$url_site?>?r=administradoras/cadastro-administradoras" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
           foreach($administradoras as $value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nomeDaAdm']?></td>
                <td><?=maskController::Mask($value['cnpj'],'cnpj')?></td>
                <td><?=Yii::$app->formatter->format($value['dataCadastro'],'date')?></td>
                <td>
                    <a href="<?=$url_site?>?r=administradoras/edita-administradoras&id=<?=$value['id']?>"name="id" class="text-white openModal"><i class="bi bi-pen-fill"></i></a> 
                    <a href="<?=$url_site?>?r=administradoras/deleta-administradoras&id=<?=$value['id']?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerAdm"><i class="bi bi-trash3-fill"></i></a>
                </td>
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="3" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$paginacao->totalCount?></small></td>
    
            </tr>
            
        </table>
        <div class="row">
            <?=LinkPager::widget(
                [
                'pagination'=>$paginacao,
                'linkContainerOptions'=>[
                    'class'=>'page-item'
                ],
                'linkOptions'=>[
                    'class'=>'page-link'
                ],
                'disabledListItemSubTagOptions'=>[
                    'class'=>'page-link'                
                ]
                ]
            )
                ?>

        </div>
    </div>
</div>
<?=ModalComponent::modal()?>