<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\UsuariosModel;
use yii;

class UsuariosController extends Controller{
    
    public function actionListarUsuarios(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = UsuariosModel::find();

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $usuarios = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-usuarios', [
            'usuarios'=>$usuarios,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroUsuarios(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-usuarios');
    }
    public function actionRealizaCadastroUsuarios(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new UsuariosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['usuarios/listar-usuarios']);
        }
        return $this->render(['cadastro-usuarios']);
    }
}
?>