<?

use app\components\ModalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;


$url_site = url::base($schema = true);
?>


<div class="col-12 text-center">
    <h1>Lista de Pets</h1>
</div>
<form id="filtro" method="GET" class="form-row mt-5 justify-content-center">
    <div class="col-12 col-sm-10">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text text-white bg-dark">Nome Do Morador:</div>
        </div>
        <input type="hidden" name="r" value="pets/search-pets">
        <input type="search"aria-label="Search" class="form-control" name="termo">
      </div>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Pesquisar</button>
        <a href="<?=$url_site?>?r=pets/listar-pets" class="btn btn-danger">Cancelar</a>
    </div>
    </form>
<div class="row">
    <div class="col-12 col-md-12">
        <table class="table table-responsive-md table-dark table-striped"id="listaPet">
            <th>Nome do pet</th>
            <th>Tipo</th>
            <th>Dono/Morador</th>
            <th>Condominio</th>
            <th><a href="<?=$url_site?>?r=pets/cadastro-pets" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
           foreach($pets as $value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nomeDoPet']?></td>
                <td><?=$value['tipo']?></td>
                <td><?=$value['morador']?></td>
                <td><?=$value['condominio']?></td>
                <td>
                    <a href="<?=$url_site?>?r=pets/edita-pets&id=<?=$value['id']?>"name="id" class="text-white openModal"><i class="bi bi-pen-fill"></i></a> 
                    <a href="<?=$url_site?>?r=pets/deleta-pets&id=<?=$value['id']?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerPet"><i class="bi bi-trash3-fill"></i></a>
                </td>
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="3" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$paginacao->totalCount?></small></td>
    
            </tr>
            
        </table>
        <div class="row">
            <?=LinkPager::widget(
                [
                'pagination'=>$paginacao,
                'linkContainerOptions'=>[
                    'class'=>'page-item'
                ],
                'linkOptions'=>[
                    'class'=>'page-link'
                ],
                'disabledListItemSubTagOptions'=>[
                    'class'=>'page-link'                
                ]
                ]
            )
                ?>

        </div>
    </div>
</div>
<?=ModalComponent::modal()?>