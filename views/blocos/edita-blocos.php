<?

use app\components\selectedComponent;
use yii\helpers\url;
use app\controllers\CondominiosController;

?>

<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Edição</h1>
    </div>
</div>

<form class="form-row formBloco" action="<?= Url::to(['blocos/realiza-edicao-blocos'])?>" method="POST">
    <div class="col-12 col-md-12 text-center from-group">
    <select class="col-6 text-center custom-select bg-custom" name="condominio">
           <option>condominio</option>
           <? foreach (CondominiosController::listaCondominiosSelect() as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>"<?= selectedComponent::isSelected($val['id'],$edit['condominio']) ?>><?=$val['nomeDoCondominio']?></option>
            <? } ?>
       </select>
    </div>
    <div class="col-12 col-md-12 d-flex justify-content-center from-group mt-2">
        <input class="col-6 text-center form-control bg-custom mb-2" type="text" name="nomeDoBloco" placeholder="Nome do bloco" value="<?=$edit['nomeDoBloco']?>"required>
    </div>
    <div class="col-12 col-md-12 d-flex justify-content-center from-group">
        <input class="col-6 text-center form-control bg-custom" type="text" name="andar" placeholder="Quantos andares?" value="<?=$edit['andar']?>"required>
    </div>
    <div class="col-12 col-md-12 d-flex justify-content-center from-group mt-2">
        <input class="col-6 text-center form-control bg-custom" type="text" name="unidades"placeholder="Quantidade de unidades por andar" value="<?=$edit['unidades']?>"required>
    </div>
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
       
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
    </div>
</form>
