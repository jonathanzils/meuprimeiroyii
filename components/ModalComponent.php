<?
namespace app\components;

use yii\base\Component;

class modalComponent extends component{
    public static function modal(){

        $estrutura = "<div class=\"modal fade\" id=\"modalComponent\" data-backdrop=\"static\" data-keyboard=\"false\" tabindex=\"-1\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-xl\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header bg-custom\">
       
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body bg-custom\">
        ...
      
    </div>
  </div>
</div>";

     return $estrutura;   
    }
}
?>