<?
namespace app\controllers;

use yii;
use yii\base\controller;

class userLogado extends controller{

    public function isLogado(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
    }
}
?>