<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_201256_blocos
 */
class m220526_201256_blocos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_bloco',[
            'id'=>Schema::TYPE_PK,
            'condominio'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'nomeDoBloco'=>Schema::TYPE_STRING . ' NOT NULL',
            'andar'=>Schema::TYPE_STRING . ' NOT NULL',
            'unidades'=>Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->addForeignKey('from_cond','jz_bloco','condominio','jz_condominio','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('from_cond','jz_bloco');
        $this->dropTable('jz_bloco');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_201256_blocos cannot be reverted.\n";

        return false;
    }
    */
}
