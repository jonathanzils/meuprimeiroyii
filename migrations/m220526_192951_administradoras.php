<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_192951_administradoras
 */
class m220526_192951_administradoras extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_adm',[
            'id'=>Schema::TYPE_PK,
            'nomeDaAdm'=>Schema::TYPE_STRING,
            'cnpj'=>Schema::TYPE_STRING,
            'dataCadastro'=>$this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
        

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('jz_adm');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_192951_administradoras cannot be reverted.\n";

        return false;
    }
    */
}
