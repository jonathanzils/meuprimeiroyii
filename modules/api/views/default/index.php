<h1>Orientações de uso da API</h1>
<div class="accordion" id="accordionExample">

<div class="card">
    <div class="card-header" id="headingZero">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
          OBTER TOKEN PARA POSTS (OBRIGATÓRIO)
        </button>
      </h2>
    </div>

    <div id="collapseZero" class="collapse" aria-labelledby="headingZero" data-parent="#accordionExample">
      <div class="card-body">
          <p><code>METHOD: </code><small>GET</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/default/get-token-post</small></p>
          <p><code>PARAMS: </code><small>null</small></p>
      </div>
    </div>
  </div>

  <!--  -->
  
  <div class="card">
    <div class="card-header" id="headingdois">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsedois" aria-expanded="true" aria-controls="collapsedois">
          LISTAR TODAS ADMINISTRADORAS
        </button>
      </h2>
    </div>

    <div id="collapsedois" class="collapse" aria-labelledby="headingdois" data-parent="#accordionExample">
      <div class="card-body">
          <p><code>METHOD: </code><small>GET</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/administradoras/get-all</small></p>
          <p><code>PARAMS: </code><small>null</small></p>
      </div>
    </div>
  </div>

<!--  -->

  <div class="card">
    <div class="card-header" id="headingtres">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapsetres" aria-expanded="false" aria-controls="collapsetres">
          LISTAR UMA ADMINISTRADORA
        </button>
      </h2>
    </div>

    <div id="collapsetres" class="collapse" aria-labelledby="headingtres" data-parent="#accordionExample">
      <div class="card-body">
          <p><code>METHOD: </code><small>GET</small></p>
          <p><code>URL: </code><small>http://localhost/meuprimeiroyii/web/index.php?r=api/administradoras/get-one</small></p>
          <p><code>PARAMS: </code><small>id (int)</small></p>
      </div>
    </div>
  </div>
</div>