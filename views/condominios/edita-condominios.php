<?

use app\components\estadosComponent;
use app\components\mascaraComponent;
use app\components\selectedComponent;
use yii\helpers\url;
use app\controllers\AdministradorasController;

?>

<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Edição</h1>
    </div>
</div>

<form class="form-row formCondominio" action="<?= Url::to(['condominios/realiza-edicao-condominios'])?>" method="POST">
    <div class="col-12 col-md-12 text-center">
        <label for="nomeDoCondominio">Nome do condominio</label>
        <input class="col-12 form-control bg-custom text-center " type="text" name="nomeDoCondominio" placeholder="Nome do condominio" value="<?=$edit['nomeDoCondominio']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="bloco">Qtd. blocos</label>
        <input class="col-12 text-center  form-control bg-custom" type="text" name="bloco" placeholder="Quantidade de blocos" value="<?=$edit['bloco']?>">
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="cep">CEP</label>
        <input class="col-12 text-center  form-control bg-custom cep" type="text" name="cep" placeholder="CEP" value="<?=mascaraComponent::mascara($edit['cep'], 'cep')?>">
    </div>
    
    <div class="col-12 col-md-12 text-center">
    <label for="logradouro">Logradouro</label>
        <input class="col-12 text-center  form-control logradouro" type="text" name="logradouro"placeholder="Logradouro"value="<?=$edit['logradouro']?>">
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="numero">Nº</label>
        <input class="col-12 text-center  form-control" type="text" name="numero"placeholder="N°"value="<?=$edit['numero']?>">
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="bairro">Bairro</label>
        <input class="col-12 text-center  form-control bairro" type="text" name="bairro"placeholder="Bairro"value="<?=$edit['bairro']?>">
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="cidade">Cidade</label>
        <input class="col-12 text-center  form-control cidades" type="text" name="cidade"placeholder="cidade"value="<?=$edit['cidade']?>">
    </div>
    <div class="col-12 col-md-12 text-center">
    <label for="estado">Estado</label>
       <select class="col-12 col-md-12 text-center custom-select fromEstado" name="estado">
           <option>estado</option>
           <?foreach(estadosComponent::estados() as $sig=>$uf){?>
          <option value="<?=$sig?>"<?=selectedComponent::isSelected($sig, $edit['estado'])?>><?=$uf?></option>
            <?}?>
       </select>
    </div>
    <div class="col-12 col-md-12  text-center">
    <label for="adm">Administradora</label>
        <select class="col-12 col-sm-12 text-center custom-select" name="adm">
        <? foreach (AdministradorasController::listaAdministradorasSelect() as $adm) { ?>
                    <option value="<?= $adm['id'] ?>"<?= selectedComponent::isSelected($adm['id'],$edit['adm']) ?>><?=$adm['nomeDaAdm']?></option>
                <? } ?>
        </select>

    </div>
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
   
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>