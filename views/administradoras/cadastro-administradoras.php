<?

use yii\helpers\Url;

?>
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formAdm" action="<?echo Url::to(['administradoras/realiza-cadastro-administradoras'])?>" method="POST">
   
    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-12 text-center form-control bg-custom mb-2" type="text" name="nomeDaAdm" placeholder="Nome do administradora" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-12 text-center form-control bg-custom" type="text" name="cnpj" placeholder="CNPJ" value=""required>
    </div>

    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    
    <div class="col-12 col-md-12 text-center">
        <button type="submit" class="btn btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
