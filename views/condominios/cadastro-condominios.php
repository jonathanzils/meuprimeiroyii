<?

use app\components\estadosComponent;
use yii\helpers\Url;
use app\controllers\AdministradorasController;

?>

<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Condominio</h1>
    </div>
</div>

<form class="form-row formCondominio" action="<?= Url::to(['condominios/realiza-cadastro-condominios'])?>" method="POST">
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom" type="text" name="nomeDoCondominio" placeholder="Nome do condominio" value=""required>
    </div>
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom" type="text" name="bloco" placeholder="Quantidade de blocos" value="">
    </div>
    <div class="col-12 col-sm-12 ">
        <input class="col-12 text-center form-control bg-custom cep" type="text" name="cep" placeholder="CEP" value="">
    </div>
    
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom logradouro" type="text" name="logradouro"placeholder="Logradouro"value="">
    </div>
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom" type="text" name="numero"placeholder="N°"value="">
    </div>
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom bairro" type="text" name="bairro" placeholder="Bairro"value="">
    </div>
    <div class="col-12 col-sm-12">
        <input class="col-12 text-center form-control bg-custom cidades" type="text" name="cidade" placeholder="cidade"value="">
    </div>
    <div class="col-12 col-sm-12">
       <select class="col-12 col-sm-12 custom-select bg-custom mt-2 text-center fromEstados" name="estado">
           <option>estado</option>
           <?foreach(estadosComponent::estados() as $sig=>$uf){?>
          <option value="<?=$sig?>"><?=$uf?></option>
            <?}?>
       </select>
    </div>
    <select class="col-12 col-sm-12 text-center custom-select bg-custom mt-2 " name="adm">
        <option >Administradora</option>
        <?foreach(AdministradorasController::listaAdministradorasSelect() as $adm){?>
        <option value="<?=$adm['id']?>"><?=$adm['nomeDaAdm'];?></option>
        <?}?>
    </select>
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-sm-12 text-center">
        
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>