<?
namespace app\models;

use yii\db\ActiveRecord;

class UnidadesModel extends ActiveRecord{

    public static function tablename(){
        return 'jz_unidade';
    }
    public function rules(){
        return [
            [['condominio','bloco','nomeDaUnidade','metragem','vagas'], 'required'],
        
        ];
    }
}

?>