<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220526_205038_usuarios
 */
class m220526_205038_usuarios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jz_usuarios',[
            'id'=>Schema::TYPE_PK,
            'nome'=>Schema::TYPE_STRING,
            'usuario'=>Schema::TYPE_STRING,
            'senha'=>Schema::TYPE_STRING,
            'dataCadastro'=>Schema::TYPE_TIMESTAMP
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jz_usuarios');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220526_205038_usuarios cannot be reverted.\n";

        return false;
    }
    */
}
