<?

use app\components\ModalComponent;
use app\controllers\AdministradorasController;
use app\controllers\CondominiosController;
use app\controllers\MaskController;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$url_site = url::base(true);
?>

<div class="col-12 text-center">
    <h1>Lista de condominios</h1>
</div>
    <form id="filtro" method="GET" class="form-row mt-5 justify-content-center">
    <div class="col-12 col-sm-10">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text text-white bg-dark">Nome Do Condominio:</div>
        </div>
        <input type="hidden" name="r" value="condominios/search-cond">
        <input type="search"aria-label="Search" class="form-control" name="termo">
      </div>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Pesquisar</button>
        <a href="<?=$url_site?>?r=condominios/listar-condominios" class="btn btn-danger">Cancelar</a>
    </div>
    </form>
<div class="row">
    <div class="col-12 col-md-12 ">


        <table class="table table-responsive-md table-dark table-striped" id="listaCondominios">
            <th>Condominio</th>
            <th>Qtd. de blocos</th>
            <th>Logradouro</th>
            <th>N°</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>estado</th>
            <th>CEP</th>
            <th>Adiministradora</th>
            <th><a href="<?= $url_site ?>?r=condominios/cadastro-condominios" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?

            foreach ($condominios as $value) {
            ?>
                <tr data-id="<?= $value['id'] ?>">
                    <td><?= $value['nomeDoCondominio'] ?></td>
                    <td><?= $value['bloco'] ?></td>
                    <td><?= $value['logradouro'] ?></td>
                    <td><?= $value['numero'] ?></td>
                    <td><?= $value['bairro'] ?></td>
                    <td><?= $value['cidade'] ?></td>
                    <td><?= $value['estado'] ?></td>
                    <td><?= maskController::Mask($value['cep'], 'cep') ?></td>
                    <td><?= $value['adm'] ?></td>
                    <td>
                        <a href="<?= $url_site ?>?r=condominios/edita-condominios&id=<?= $value['id'] ?>" name="id" class="text-white openModal"><i class="bi bi-pen-fill"></i></a>
                        <a href="<?= $url_site ?>?r=condominios/deleta-condominios&id=<?= $value['id'] ?>" name="remove" data-id="<?= $value['id'] ?>" class="text-white mr-4 removerCondominio"><i class="bi bi-trash3-fill"></i></a>
                    </td>

                </tr>
            <? } ?>
            <tr>
                <td colspan="4">&nbsp;</td>

                <td colspan="6" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?= $paginacao->totalCount ?> </small></td>

            </tr>
        </table>

        <div class="row">
            <?= LinkPager::widget(
                [
                    'pagination' => $paginacao,
                    'linkContainerOptions' => [
                        'class' => 'page-item'
                    ],
                    'linkOptions' => [
                        'class' => 'page-link'
                    ],
                    'disabledListItemSubTagOptions' => [
                        'class' => 'page-link'
                    ]
                ]
            )
            ?>

        </div>

    </div>
</div>
<?=ModalComponent::modal() ?>