<?php

namespace app\controllers;

use app\models\LoginForm;
use yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\User;

class SiteController extends Controller{

    public function actionIndex(){
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);

        }
        return $this->render('home');
    }
    
    

    public function actionLogin(){
        $this->layout = false;
        if(!Yii::$app->user->isGuest){
            return $this->goHome();
        }
        $request = \yii::$app->request;

        if($request->isPost){
            $identity = LoginForm::findOne(['usuario'=> $request->post('usuario'), 'senha' =>$request->post('senha')]);
            if($identity){
                yii::$app->user->login($identity);
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['login',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Login não confere',
                    'url'=>'site/login'
                ]]);
            }
        }
        return $this->render('login');
    }
    public function actionLogout(){
        yii::$app->user->logout();
        return $this->redirect(['site/login']);
    }
      
}

    
