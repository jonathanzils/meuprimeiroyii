<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

?>

<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formUnidade" action="<?= Url::to(['unidades/realiza-cadastro-unidades'])?>" method="POST">

    <div class="col-12 col-md-12 text-center from-group mt-2 mb-2">
        <select name="condominio"class="fromCondominio col-12 text-center custom-select bg-custom">
            <option>Condominio</option>
            <?foreach(CondominiosController::listaCondominiosSelect() as $ch=>$val){?>
            <option value="<?=$val['id']?>"><?=$val['nomeDoCondominio']?></option>
            <?}?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="bloco"class="fromBloco col-12 text-center custom-select bg-custom" value="8">
       
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-12 form-control bg-custom text-center mb-2" type="text" name="nomeDaUnidade" placeholder="Nome do unidade" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-12 form-control bg-custom text-center" type="text" name="metragem" placeholder="Metragem da unidade" value=""required>
    </div>
    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-12 form-control bg-custom text-center" type="text" name="vagas"placeholder="Qtd. de vagas de garagem" value=""required>
    </div>
    <input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
    <div class="col-12 col-md-12 text-center">
        
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
