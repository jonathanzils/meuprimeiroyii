<?
namespace app\modules\api\controllers;

use app\models\AdministradorasModel;
use app\models\CondominiosModel;
use app\models\ConselhosModel;
use yii\web\Controller;

class ConselhosController extends Controller{
    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    public function actionGetAll(){
        $qry = (new \yii\db\Query())
        ->select('con.id,
        cond.nomeDoCondominio AS condominio,
        con.nome AS nome,
        con.funcao AS funcao')
        ->from(ConselhosModel::tableName().' con')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = con.condominio');
        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i = 0;

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeDaAdm'] = $d['nomeDaAdm'];
                $dados['resultSet'][$i]['cnpj'] = $d['cnpj'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = (new \yii\db\Query())
        ->select('con.id,
        cond.nomeDoCondominio AS condominio,
        con.nome AS nome,
        con.funcao AS funcao')
        ->from(ConselhosModel::tableName().' con')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = con.condominio');
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['resultSet'][0]['id'] = $d['id'];
            $dados['resultSet'][0]['nomeDaAdm'] = $d['nomeDaAdm'];
            $dados['resultSet'][0]['cnpj'] = $d['cnpj'];
        }else{
            $dados['endPoint']['status'] = 'nodata';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }
        return json_encode($dados);
    }
    
    public function actionRegisterConselhos(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){

                $model = new ConselhosModel();
                $model->attributes = $request->post();
                $model->save();
                
                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';
    
                return json_encode($dados);
            }
        } catch (\throwable $e) {
            $dados['endPoint']['status'] = 'danger';
            $dados['endPoint']['msg'] = 'Não existe dados para este consumo.';
            return json_encode($dados);
        }

    }
    public function actionEditConselhos(){
        $request = \yii::$app->request;
       try {
           if($request->isPost){
               $model = ConselhosModel::findOne($request->post('id'));
               $model->attributes = $request->post();
               $model->update();

               $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro alterado com sucesso.';
                return json_encode($dados);
           }
       } catch (\throwable $e) {
        $dados = [];
        $dados['endPoint']['status'] = 'noData';
        $dados['endPoint']['msg'] = 'Não existe dados para este consumo.';

        return json_encode($dados);
       }
    }
    public function actionDeleteConselhos(){
        $request = \yii::$app->request;

    try {
        if($request->isPost){
            $model = ConselhosModel::findOne($request->post('id'));
            $model->delete();
            $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                return json_encode($dados);
        }
    } catch (\throwable $e) {
        $dados = [];
        $dados['endPoint']['status'] = 'danger';
        $dados['endPoint']['msg'] = 'Registro não pode ser deletado';

        return json_encode($dados);
       }
    }
    
}
?>
