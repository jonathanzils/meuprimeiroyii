<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

?>

<h1 class="text-center mb-5">Cadastro de conselhos</h1>

<form class="form-row formConselho" action="<?= Url::to(['conselhos/realiza-cadastro-conselhos'])?>" method="POST">

<div class="col-12 text-center form-group ">
    <select name="condominio" class="col-12 text-center bg-custom mb-2 form-control custom-select">
    <option>condominio</option>
    <?foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
        <option value="<?=$cond['id']?>"><?=$cond['nomeDoCondominio'];?></option>
        <?}?>
</div> 

<div class="col-12 text-center form-group">
    <input class="col-12 text-center form-control bg-custom" type="text" name="nome" placeholder="nome" value=""required>
</div>

<div class="col-12 col-md-12 text-center form-group">
<select name="funcao" class="col-12 text-center mb-2 bg-custom custom-select">
    <option>função</option>
        <option value="1">Sindico</option>
        <option value="2">Sub sindico</option>
        <option value="3">Conselheiro</option>
    </select>
</div>
<input type="hidden" name="<?= \Yii::$app->request->csrfParam;?>" value="<?= yii::$app->request->csrfToken;?>">
<div class="col-12 col-md-12 text-center">
    
    <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
    
</div>
</form>