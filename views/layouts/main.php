<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\components\alertComponent;
use app\controllers\userLogado;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\FontAwesomeAsset;

FontAwesomeAsset::register($this);
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="images/Logo-site.png" width="100px"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            [
                'label'=>'listas',
                'items'=>[
                    [
                        'label'=> 'Administradoras',
                        'url'=> ['administradoras/listar-administradoras']
                    ],
                    [
                        'label'=> 'Condominios',
                        'url'=> ['condominios/listar-condominios']
                    ],
                    [
                        'label'=> 'Conselhos',
                        'url'=> ['conselhos/listar-conselhos']
                    ],
                    [
                        'label'=> 'Blocos',
                        'url'=> ['blocos/listar-blocos']
                    ],
                    [
                        'label'=> 'Unidades',
                        'url'=> ['unidades/listar-unidades']
                    ],
                    [
                        'label'=> 'Moradores',
                        'url'=> ['moradores/listar-moradores']
                    ],
                    [
                        'label'=> 'Pets',
                        'url'=>['pets/listar-pets']
                    ]
                ],
            ],
            [
                'label'=>'Cadastrar',
                'items'=>[
                    [
                        'label'=> 'Administradoras',
                        'url'=> ['administradoras/cadastro-administradoras']
                    ],
                    [
                        'label'=> 'Condominios',
                        'url'=> ['condominios/cadastro-condominios']
                    ],
                    [
                        'label'=> 'Conselhos',
                        'url'=> ['conselhos/cadastro-conselhos']
                    ],
                    [
                        'label'=> 'Blocos',
                        'url'=> ['blocos/cadastro-blocos']
                    ],
                    [
                        'label'=> 'Unidades',
                        'url'=> ['unidades/cadastro-unidades']
                    ],
                    [
                        'label'=> 'Moradores',
                        'url'=> ['moradores/cadastro-moradores']
                    ],
                    [
                        'label'=>'Pets',
                        'url'=>['pets/cadastro-pets']
                    ]
                    ],
                ],
                [                 
                    'label'=>'lista de usuarios',
                    'url'=>['usuarios/listar-usuarios']                
                ],
                yii::$app->user->isGuest ? (
                    ['label'=>'login','url'=>['/site/login']]
                ) : (
                    '<li>'
                    .html::beginForm(['/site/logout'], 'post',['class'=>'form-inline'])
                    .html::submitButton(
                        'logout ('.yii::$app->user->identity->usuario . ')',
                        ['class'=>'btn btn-link logout']
                    )
                    .Html::endForm()
                    .'</li>'
                )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 mt-5">
    <div class="container">
       
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto bg-dark py-3 fixed-bottom text-muted">
    <div class="container">
        <div class="float-left">
             <p class="">&copy;ITM APPS <?= date('Y') ?></p>
        </div>
        <div class="float-right">
            
        <p><?= Yii::powered() ?></p>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
