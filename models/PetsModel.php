<?
namespace app\models;

use yii\db\ActiveRecord;

class PetsModel extends ActiveRecord{

    public static function tablename(){
        return 'jz_pets';
    }
 
    public function rules(){
        return [
            [['nomeDoPet','nomeDoPet'], 'required'],
            [['tipo','tipo'], 'required'],
            [['morador','morador'], 'required'],
            [['condominio','condominio'], 'required']
        ];
    }
}

?>