
<?

use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$url_site = url::base(true);
?>


<div class="col-12 text-center">
    <h1>Lista de Conselheiros</h1>
</div>
<form id="filtro" method="GET" class="form-row mt-5 justify-content-center">
    <div class="col-12 col-sm-10">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text text-white bg-dark">Nome Do Condominio:</div>
        </div>
        <input type="hidden" name="r" value="conselhos/search-conselhos">
        <input type="search"aria-label="Search" class="form-control" name="termo">
      </div>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Pesquisar</button>
        <a href="<?=$url_site?>?r=conselhos/listar-conselhos" class="btn btn-danger">Cancelar</a>
    </div>
    </form>
<div class="row">
    <div class="col-12 col-md-12">
        <table class="table table-responsive-md table-dark table-striped"id="listaConselho">
            <th>Condominio</th>
            <th>nome</th>
            <th>função</th>
            
            <th><a href="<?=$url_site?>?r=conselhos/cadastro-conselhos" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
            
            foreach($conselhos as $ch=>$value){
                ?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['nome']?></td>
                <td><?=$value['funcao']?></td>
                
                
                <td>
                    <a href="<?=$url_site?>?r=conselhos/edita-conselhos&id=<?=$value['id']?>"name="id" class="text-white openModal"><i class="bi bi-pen-fill"></i></a> 
                    <a href="<?=$url_site?>?r=conselhos/deleta-conselhos&id=<?=$value['id']?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerConselho"><i class="bi bi-trash3-fill"></i></a>
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="3" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$paginacao->totalCount?></small></td>

            </tr>
        </table>
        <div class="row">
            <?=LinkPager::widget(
                [
                'pagination'=>$paginacao,
                'linkContainerOptions'=>[
                    'class'=>'page-item'
                ],
                'linkOptions'=>[
                    'class'=>'page-link'
                ],
                'disabledListItemSubTagOptions'=>[
                    'class'=>'page-link'                
                ]
                ]
            )
                ?>
    </div>
    </div>
</div>
<?=ModalComponent::modal()?>
