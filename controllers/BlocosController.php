<?
namespace app\controllers;

use app\models\CondominiosModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocosModel;
use yii;

class BlocosController extends Controller{
    public function actionListarBlocos(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        // $query = BlocosModel::find();
        $query = (new \yii\db\Query())
        ->select('bloco.id,
        cond.id as idcond,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco,
        bloco.andar,
        bloco.unidades')
        ->from(BlocosModel::tableName().' bloco')
        ->innerJoin(CondominiosModel::tableName().' cond','cond.id = bloco.condominio');

        $paginacao = new Pagination([
            'defaultPageSize'=>5,
            'totalCount'=>$query->count(),
        ]);

        $blocos = $query->orderBy('nomeDoBloco')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();

        return $this->render('listar-blocos', [
            'blocos'=>$blocos,
            'paginacao'=>$paginacao,
        ]);
    }
    public function actionCadastroBlocos(){
        if(yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        return $this->render('cadastro-blocos');
    }
    public function actionRealizaCadastroBlocos(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new BlocosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['blocos/listar-blocos']);
        }
        return $this->render(['cadastro-blocos']);
    }
    
    public static function listaBlocosSelect(){
        $query = BlocosModel::find();
        
        return $query->orderBy("nomeDoBloco")->all();
    }
    public static function blocosSelect($from){
        $query = BlocosModel::find();
        $data = $query->where(['condominio'=>$from])->orderBy('nomeDoBloco')->all();
        return $data;

    }
    public function actionListaBlocoApi(){
        $request = \yii::$app->request;
        $query = BlocosModel::find();
        $data = $query->where(['condominio'=>$request->post()])->orderBy('nomeDoBloco')->all();
        $dados = array();
        $i = 0;
        foreach($data as $d){
        $dados[$i]['id'] = $d['id'];
        $dados[$i]['nomeDoBloco'] = $d['nomeDoBloco'];
        $i++;
        }
        return json_encode($dados);
    }
    public function actionEditaBlocos(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = BlocosModel::find();
            $blocos = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-blocos',[
            'edit'=>$blocos
        ]);
    }
    public function actionRealizaEdicaoBlocos(){
        $request = \yii::$app->request; 
        if($request->isPost){
            
            $model = BlocosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
         
            if($model->update()){
               return $this->redirect(['blocos/listar-blocos']);
           }else{
               return $this->redirect(['blocos/listar-blocos','msg'=>'erro']);
           }
            

        }

    }
    public function actionDeletaBlocos(){
        $request = \yii::$app->request;
        if($request->isGet){
            $model = BlocosModel::findOne($request->get('id'));
            if($model->delete()){
                return $this->redirect(['blocos/listar-blocos',
                'myAlert'=>[
                    'type'=>'success',
                    'msg'=>'Registro deletado com sucesso'
                ]]);
            }else{
                return $this->redirect(['blocos/listar-blocos',
                'myAlert'=>[
                    'type'=>'danger',
                    'msg'=>'Não foi possivel deletar o registro'
                ]]);
            }
        }
    }
    public function actionSearchBlocos(){
        $request = \yii::$app->request;
        if($request->isGet){
           
            $query = (new \yii\db\Query())
            ->select('bloco.id,
            cond.id as idcond,
            cond.nomeDoCondominio AS condominio,
            bloco.nomeDoBloco,
            bloco.andar,
            bloco.unidades')
            ->from(BlocosModel::tableName().' bloco')
            ->innerJoin(CondominiosModel::tableName().' cond','cond.id = bloco.condominio')
            ->where(['like','cond.nomeDoCondominio', $request->get('termo')]);
    
            
            $paginacao = new Pagination([
                'defaultPageSize'=>5,
                'totalCount'=>$query->count(),
            ]);
    
    
    
            $blocos = $query->orderBy('nomeDoCondominio')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->All();


        }
        return $this->render('listar-blocos',[
            'blocos'=>$blocos,
            'paginacao'=>$paginacao
        ]);
    }
}
?>